% One Dimensional wave
% 2015-03-08
% Jeng-Hau Lin
clc;
clear all;
close all;
set(0,'DefaultFigureColor',[1 1 1]);

t = (0.01:0.01:1)*1e-9;
h = t(2)-t(1);
epislon0 = 8.8541878176e-12;
mu0 = pi*4e-7;
ita = sqrt(mu0/epislon0);
c = 1/sqrt(mu0*epislon0);

N_spatial = 500;
phy_length = 2;
a = phy_length/N_spatial;

spatial = phy_length/N_spatial*(1:N_spatial);
Os = zeros(1,N_spatial);



Mu = eye(N_spatial)*mu0;
Me = eye(N_spatial)*epislon0;
Ms = zeros(N_spatial,N_spatial);

I3 = eye(N_spatial);

Ktmp = eye(N_spatial+1);
Ktmp2 = -Ktmp + circshift(Ktmp, [0 1]);
K = Ktmp2(1:end-1,2:end);
% K = [-1  1 0;...
%      0  -1 1;...
%      0   0 -1];

% return;
A = [zeros(N_spatial)    (Mu)\K/a;...
     -(Me)\K'/a          (Me)\Ms];
% returnspy
V0 = [0; zeros(N_spatial-1,1); 1; zeros(N_spatial-1,1)];
% V0 = [1;...
%       0;
%       0;
%       0;
%       0;
%       0;];
  

Tol = 1e-3;
m = 200;
y1 = V0;
% e_field1 = zeros()
tic
for ind= 1:length(t)
    y1 = expm(-h*A)*y1;

    h_field1(ind) = y1(5);
    e_field1(ind) = y1(N_spatial+5);
    
    plot3(spatial,Os,y1(N_spatial+1:end)');%,...
    hold on;
    plot3(spatial,-y1(1:N_spatial)',Os,'r--');
 
    xlabel('space (meter)');
    zlabel('electric field (V/m)');
    ylabel('magnetic field (mA/m)');

    xlim([0 c*t(end)]);
    ylim([-1 1]*1e-3);
    zlim([-0.5 0.5]);
    box on;
    hold off;
%     TEM1(ind) = getframe;
    frame = getframe(1);
    im = frame2im(frame);
    [imind,cm] = rgb2ind(im,256);
      if ind == 1;
           imwrite(imind,cm,'./TEMwave01.gif','gif', 'Loopcount',inf);
      else
          imwrite(imind,cm,'./TEMwave01.gif','gif','WriteMode','append');
      end
end
fprintf('ME time: %0.2f sec. \n',toc);

return;
tic
y2 = V0;
for ind= 1:length(t)
    [y2, mj] = expm_Arnoldi(A,y2,h,Tol,m);

%     fprintf('j = %d .\n', j);
    h_field2(ind) = y2(5);
    e_field2(ind) = y2(N_spatial+5);
        
    plot3(spatial,Os,y2(N_spatial+1:end)');%,...
    hold on;
    plot3(spatial,-y2(1:N_spatial)',Os,'r--');
 
    xlabel('space (meter)');
    zlabel('electric field (V/m)');
    ylabel('magnetic field (mA/m)');

    xlim([0 c*t(end)]);
    ylim([-1 1]*1e-3);
    zlim([-0.5 0.5]);
    box on;
    hold off;
    TEM2(ind) = getframe; 
end
fprintf('Kylov ME time: %0.2f sec.\n',toc);

% return;

tic

t = [0 t];

e_field1 = [V0(2) e_field1];
h_field1 = [V0(4) h_field1];
e_field2 = [V0(2) e_field2];
h_field2 = [V0(4) h_field2];

figure(2)
subplot(2,1,1)
plot(t,e_field1, t,e_field2,'--');
ylim([-1 1]);
subplot(2,1,2)
plot(t,h_field1, t,h_field2,'--');
ylim([-1 1]);
toc

figure(3)
movie(TEM1);
movie2avi(TEM1,'./TEMwave01.avi');

figure(4)
movie(TEM2);
movie2avi(TEM2,'./TEMwave02.avi');
