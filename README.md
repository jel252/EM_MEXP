## EM_MEXP

This is a demo of using matrix exponential to solve Maxwell's Equations in a discrete form.
I used M.A. Botchev's implementation[1] of matrix exponential solver to simulate a transpose electromagnetic (TEM) wave propagating through a cubic space.

I direct clone expm_Arnodi.m from Botchev's git repository.
As long as you cite my repository, you are free to use my code for any non-commercial purpose.

## Simulating TEM wave

Following the arrangement of discrete Maxwell's Eq. we can compose a matrix with the electric field, magnetic flux, and excitation voltage.
Betchev's expm function can take the aforementioned matrix as an input and return the output EM waveform as shown in the following figure.
<br><img src="https://gitlab.com/jel252/EM_MEXP/raw/master/Img/TEMwave01.gif"  width="50%" ><br>



## Reference
[1] Botchev, Mike A. "A short guide to exponential Krylov subspace time integration for Maxwell's equations." (2012).
